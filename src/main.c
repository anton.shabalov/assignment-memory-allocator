#include "mem.h"
#include "mem_internals.h"

static struct block_header* getBlock(void * block);
static struct block_header* getBlock(void* block) {
    return (struct block_header *) ((uint8_t *) block - offsetof(struct block_header, contents));
}

static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}

bool t1(struct block_header *block) {
    printf("%s", "запускаем тест 1 \n");
    printf("%s", "\n Память до начала теста\n");
    debug_heap(stdout, block);
    void *block2 = _malloc(10);
    printf("%s", "\n память после создания первого блока\n");
    debug_heap(stdout, block);

    if (block2 == NULL) {
        printf("%s","\nmalloc сработал не корректно и не смог вернуть адрес ");
        return false;

    }
    if(block->is_free==true){
        printf("%s","\nне получилось поставить флаг сигнализируеющий о том, что место занято");
        return false;

    }
    if(block->capacity.bytes!=10){
        printf("%s","\n не получается выделить нужный объем памяти");
        return false;
    }
    printf("%s","\n Тест номер 1 успешно пройден\n");
    _free(block2);
    return true;
}

bool t2(struct block_header *block) {
    printf("%s","запускаем тест 2\n");
    printf("%s", "\n Память до начала теста\n");
    debug_heap(stdout, block);

    void *block1 = _malloc(74);
    void *block2 = _malloc(21);
    if (block1 == block2 ) {
        printf("%s","malloc сработал не корректно и не смог вернуть адрес \n");
        return false;
    }else{
        printf("%s", "\n память после освобождения 1-2 блока\n");
        debug_heap(stdout, block);
        if(getBlock(block1)->is_free==0&& getBlock(block2)->is_free==0){
        }else{
            printf("%s","ошибка в работе 2 теста \n" );
            return false;
        }
        _free(block2);
        printf("%s", "\n память после освобождения 2 блока\n");
        debug_heap(stdout, block);
        if(getBlock(block1)->is_free==0 && getBlock(block2)->is_free==1){
            _free(block1);
            _free(block2);
            printf("%s","тест 2 прошел, результаты сверху \n" );
            return true;
        }else{
            printf("%s","ошибка в работе 2 теста\n" );
            return false;
        }

    }
}

bool t3(struct block_header *block) {
    printf("%s","запускаем тест 3\n");
    printf("%s", "\n Память до начала теста\n");
    debug_heap(stdout, block);
    void *block1 = _malloc(15);
    void *block2 = _malloc(12);
    void *block3 = _malloc(3);
    if(block1==NULL){
        printf("%s","malloc сработал не корректно и не смог вернуть адрес \n");
        return false;
    }
    if(block2==NULL){
        printf("%s","malloc сработал не корректно и не смог вернуть адрес \n");
        return false;
    }
    if(block3==NULL){
        printf("%s","malloc сработал не корректно и не смог вернуть адрес \n ");
        return false;
    }
    if(getBlock(block1)->is_free==0&& getBlock(block2)->is_free==0&& getBlock(block3)->is_free==0){
        printf("%s", "\n память после создания 1-3 блока\n");
        debug_heap(stdout, block);
        _free(block1);
        _free(block2);
        if(getBlock(block1)->is_free==1&& getBlock(block2)->is_free==1&& getBlock(block3)->is_free==0){
            printf("%s", "\n память после освобождения 1-2 блока\n");
            debug_heap(stdout, block);
            printf("%s","тест 3 прошел, результаты сверху \n" );
            _free(block1);
            _free(block2);
            _free(block3);
            return true;
        }else{
            printf("%s","ошибка в 3 тесте \n" );
            return false;
        }
    }else{
        printf("%s","ошибка в 3 тесте\n" );
        return false;
    }

}

bool t4(struct block_header *block) {
    printf("%s","запускаем тест 4\n");
    printf("%s", "\n Память до начала теста\n");
    debug_heap(stdout, block);
    void *block1 = _malloc(5000);
    printf("%s", "\n память после создания 1 блока\n");
    debug_heap(stdout, block);

    void *block2 = _malloc(19000);
    if(block1==NULL){
        printf("%s","malloc сработал не корректно и не смог вернуть адрес \n");
        return false;
    }
    if(block2==NULL){
        printf("%s","malloc сработал не корректно и не смог вернуть адрес \n");
        return false;
    }
    printf("%s", "\n память после создания 2 блока\n");
    debug_heap(stdout, block);
    _free(block1);
    _free(block2);
    return true;
}

bool t5(struct block_header *block) {
    printf("%s","запускаем тест 5\n");
    printf("%s", "\n Память до начала теста\n");
    debug_heap(stdout, block);
    void* a=mmap((void *)((uint8_t *)block + (uint8_t)19000), 20000, PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_PRIVATE,-1, 0);
    (void) a;
    _malloc(31000);
    printf("%s", "\n память после аллокации\n");
    debug_heap(stdout, block);
    return true;
}

int main() {
    struct block_header *block =heap_init(20000);;
    if(t1(block)){
        if(t2(block)){
            if(t3(block)){
                if(t4(block)){
                    if(t5(block)){
                        printf("%s","все тесты прошли \n");
                    }else{
                        printf("%s","Произошла ошибка на 5 тесте");

                    }
                }else{
                    printf("%s","Произошла ошибка на 4 тесте");

                }
            }else{
                printf("%s","Произошла ошибка на 3 тесте");

            }
        } else{
            printf("%s","Произошла ошибка на 2 тесте");
        }


    }else{
        printf("%s","Произошла ошибка на 1 тесте");
    }


return 0;
}
