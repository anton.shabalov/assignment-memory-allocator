#define _DEFAULT_SOURCE

#include <assert.h>

#include <stdarg.h>
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "mem.h"
#include "mem_internals.h"
#include "util.h"



void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool            block_is_big_enough( size_t query, struct block_header* block ) { return block->capacity.bytes >= query; }
static size_t          pages_count   ( size_t mem )                      { return mem / getpagesize() + ((mem % getpagesize()) > 0); }
static size_t          round_pages   ( size_t mem )                      { return getpagesize() * pages_count( mem ) ; }

static void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
    *((struct block_header*)addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static inline size_t region_actual_size( size_t query ) { return size_max( round_pages( query ), REGION_MIN_SIZE ); }

extern inline bool region_is_invalid( const struct region* r );



static inline void* map_pages(void const* addr, size_t length, int additional_flags) {
    return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , -1, 0 );
}

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region  ( void const * addr, size_t query ) {
	size_t reg_size=region_actual_size(query+offsetof(struct block_header, contents));
    void* r = map_pages(addr, reg_size, MAP_FIXED_NOREPLACE|MAP_FIXED);
    struct region regions;
     regions.size=reg_size;
     regions.extends=true;
    if (r == MAP_FAILED){
        r = map_pages(addr, reg_size, 0);
        if(r == MAP_FAILED){
            return REGION_INVALID;
        }
       regions.extends=false;
    }
    block_init(r, (block_size) {reg_size}, NULL);
    regions.addr=r;
    return regions;

}

static inline void* block_after( struct block_header const* block )         ;

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;

  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

static inline bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big( struct block_header* block, size_t query ) {
    if(block_splittable(block,query)){
        block_init((void*) (struct block_header*)(block->contents + query), (block_size) {block->capacity.bytes - query}, block->next);
        block->next = (struct block_header*)(block->contents + query);
        block->capacity.bytes = query;
        return true;
    } else{
        return false;
    }
}


/*  --- Слияние соседних свободных блоков --- */

static void* block_after( struct block_header const* block )              {
  return  (void*) (block->contents + block->capacity.bytes);
}
static bool blocks_continuous (
                               struct block_header const* fst,
                               struct block_header const* snd ) {
  return (void*)snd == block_after(fst);
}

static inline bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd) {
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
    if(block->next){
        if(mergeable(block,block->next)){
            block->capacity.bytes+= size_from_capacity(block->next->capacity).bytes;
            block->next=block->next->next;
            return true;
        }else{
            return false;
        }
    }else{
        return false;
    }

}


/*  --- ... ecли размера кучи хватает --- */

struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED} type;
  struct block_header* block;
};


static struct block_search_result find_good_or_last  ( struct block_header* restrict block, size_t sz )    {
    struct block_header *cBlock = block;
    struct block_header *lBlock = block;
    struct block_search_result r;
    r.type=BSR_FOUND_GOOD_BLOCK;
    r.block=cBlock;
    while (cBlock) {
  		if(cBlock->is_free){
  			while(cBlock->next!=NULL&& try_merge_with_next(cBlock));
  			if (block_is_big_enough(sz, cBlock)) {
				r.block = cBlock;
			
				return r;
			}

  		
  		}
  		
            lBlock = cBlock;
            cBlock = cBlock->next;
        }
        r.type=BSR_REACHED_END_NOT_FOUND;
        r.block=lBlock;
        return r;
        
    }

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    struct block_search_result r = find_good_or_last(block, query);
    if (r.type == BSR_FOUND_GOOD_BLOCK) {
        split_if_too_big(r.block, query);
        r.block->is_free=false;
    }else{
        return r;
    }
    return r;

}



static struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    void* s=block_after(last);
    struct region region = alloc_region(s,query);
    if(region_is_invalid(&region)){
    	return NULL;
    }
   // block_init(region.addr, (block_size) {region.size}, NULL);
    last->next = (struct block_header*) region.addr;
    if (try_merge_with_next(last)) {
        return last;
    } else{
        return last->next;
    }

}

/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start) {
	
    if(query>BLOCK_MIN_CAPACITY){
    }else{
        query=BLOCK_MIN_CAPACITY;
    }
    struct block_search_result r = try_memalloc_existing(query, heap_start);
    if(r.type==BSR_FOUND_GOOD_BLOCK){
	return r.block;

    }else{
        if(r.type==BSR_REACHED_END_NOT_FOUND){
            struct block_header* he = grow_heap(r.block, query);
            if(he!=NULL){	
            	r = try_memalloc_existing(query, he);
            	if(r.type==BSR_FOUND_GOOD_BLOCK){
            		return r.block;
            	}else{
            		return NULL;
            	}

            }else{
             return NULL;
            }
        }
        if(BSR_CORRUPTED==r.type){
            r.block=NULL;
        }
    }
    return r.block;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

static inline struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header));
}                 
